#import "spark.h"

@implementation spark

- (void)drawSpark:(float)widthHeight {
	CGRect containerFrame = CGRectMake(0, 0, widthHeight, widthHeight);
	
	float startX = containerFrame.size.width/2;
	float startY = containerFrame.size.height/2;
	float endX;
	float endY;
	NSArray *pointsX = @[@0.0,
					  @(containerFrame.size.width/2),
					  @(containerFrame.size.width),
					  @((containerFrame.size.width/2)/2 * 3),
					  @(containerFrame.size.width),
					  @((containerFrame.size.width/2)/2 * 3),
					  @(containerFrame.size.width/2),
					  @((containerFrame.size.width/2)/2),
					  @0.0,
					  @((containerFrame.size.width/2)/2)];
	NSArray *pointsY = @[@0.0,
					  @((containerFrame.size.height/2)/2),
					  @0.0,
					  @(containerFrame.size.height/2),
					  @((containerFrame.size.height/2)/2 * 3),
					  @((containerFrame.size.height/2)/2 * 3),
					  @(containerFrame.size.height),
					  @((containerFrame.size.height/2)/2 * 3),
					  @((containerFrame.size.height/2)/2 * 3),
					  @(containerFrame.size.height/2)];
	
	
	UIImageView *drawImage = [[UIImageView alloc] init];
	drawImage.frame = CGRectMake(0, 0, containerFrame.size.width, containerFrame.size.height);
	drawImage.backgroundColor = [UIColor blueColor];
	
	UIGraphicsBeginImageContext(containerFrame.size);
	[drawImage.image drawInRect:CGRectMake(0, 0, containerFrame.size.width, containerFrame.size.height)];
	CGContextSetLineCap(UIGraphicsGetCurrentContext(), kCGLineCapRound);
	CGContextSetLineWidth(UIGraphicsGetCurrentContext(), 0.5);
	CGContextSetRGBStrokeColor(UIGraphicsGetCurrentContext(), 255.0, 255.0, 255.0, 1.0);
	CGContextBeginPath(UIGraphicsGetCurrentContext());
	for( int i = 0; i <= [pointsX count] - 1; i++) {
		endX = [pointsX[i] floatValue];
		endY = [pointsY[i] floatValue];
		CGContextMoveToPoint(UIGraphicsGetCurrentContext(), startX, startY);
		CGContextAddLineToPoint(UIGraphicsGetCurrentContext(), endX, endY);
	}
	CGContextStrokePath(UIGraphicsGetCurrentContext());
	drawImage.image = UIGraphicsGetImageFromCurrentImageContext();
	UIGraphicsEndImageContext();
	
	self.contents = (id)drawImage.image.CGImage;

}

@end
