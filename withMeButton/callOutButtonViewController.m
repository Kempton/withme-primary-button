#import "callOutButtonViewController.h"
#import "spark.h"

static float extraPadding = 20;
static float scaleReducer = 0.8;
static NSArray *particleXLocation = nil;
static NSArray *particleYLocation = nil;
static NSArray *particleDelay = nil;
static NSArray *particleXDeviate = nil;
static NSArray *particleYDeviate = nil;

static NSArray *iconGradientColors = nil;
static NSArray *iconGradientColorsDisabled = nil;
static NSArray *iconGradientColorsSelected = nil;

static UIColor *iconTopGradientColor = nil;
static UIColor *iconBottomGradientColor = nil;

static UIColor *iconDisabledTopGradientColor = nil;
static UIColor *iconDisabledBottomGradientColor = nil;

static UIColor *iconSelectedTopGradientColor = nil;
static UIColor *iconSelectedBottomGradientColor = nil;

static UIColor *backgroundColor = nil;
static dispatch_once_t onceToken;

@interface callOutButtonViewController ()

@property (nonatomic, strong) UIButton *container;
@property (strong, nonatomic) NSString* type;
@property (nonatomic) float width;
@property (nonatomic) float height;
@property (strong, nonatomic) NSString* icon;
@property (nonatomic) float iconWidth;
@property (nonatomic) float iconHeight;
@property (nonatomic) float widthEP;
@property (nonatomic) float heightEP;
@property (nonatomic) BOOL setDisabled;
@property (nonatomic) BOOL isDisabled;
@property (nonatomic) BOOL setSelectable;
@property (nonatomic) BOOL isSelected;
@property (nonatomic) BOOL animationInProgress;


@property (strong, nonatomic) CALayer *borderForLayout;
@property (strong, nonatomic) CALayer *scaleHolder;
@property (strong, nonatomic) CALayer *firstScale;
@property (strong, nonatomic) CALayer *secondScale;
@property (strong, nonatomic) CALayer *scaleMask;


@property (strong, nonatomic) CALayer *shineContainer;
@property (strong, nonatomic) CALayer *shineMovingHolderInsideContainer;
@property (strong, nonatomic) CAGradientLayer *shine;
@property (strong, nonatomic) CALayer *shineMask;


@property (strong, nonatomic) CALayer *background;
@property (strong, nonatomic) CALayer *iconImage;
@property (strong, nonatomic) CALayer *tail;

@property (strong, nonatomic) NSMutableArray *particles;
@property (strong, nonatomic) CALayer *particleHolder;
@property (strong, nonatomic) CALayer *particle1;
@property (strong, nonatomic) CALayer *particle2;
@property (strong, nonatomic) CALayer *particle3;


@property (strong, nonatomic) CAGradientLayer *iconBackground;

@property (strong, nonatomic) CABasicAnimation* tailFadeIn;
@property (strong, nonatomic) CABasicAnimation* tailFadeOut;
@property (strong, nonatomic) CABasicAnimation* tailAnimate;
@property (strong, nonatomic) CABasicAnimation* whiteScaleIn;
@property (strong, nonatomic) CABasicAnimation* whiteFadeOut;
@property (strong, nonatomic) CABasicAnimation* yellowScaleIn;
@property (strong, nonatomic) CABasicAnimation* yellowFadeOut;
@property (strong, nonatomic) CABasicAnimation* shineAnimate;

@property (strong, nonatomic) CABasicAnimation *particleFadeIn;
@property (strong, nonatomic) CABasicAnimation *particleMove;
@property (strong, nonatomic) CABasicAnimation *particleFadeOut;
@property (strong, nonatomic) CABasicAnimation *particleRotate;
@property (strong, nonatomic) CAAnimationGroup *particleGroup;

@property (strong, nonatomic) CABasicAnimation *disabledBackground;
@property (strong, nonatomic) CABasicAnimation *disabledIcon;
@property (strong, nonatomic) CABasicAnimation *selectedIcon;
@property (strong, nonatomic) CABasicAnimation *deselectedIcon;

@property (strong, nonatomic) CABasicAnimation *enabledBackground;
@property (strong, nonatomic) CABasicAnimation *enabledIcon;


@property (strong, nonatomic) CABasicAnimation *originalBackground;
@property (strong, nonatomic) CABasicAnimation *selectedBackground;

@property (nonatomic) BOOL storedDisabled;
@property (nonatomic) BOOL storedEnabled;



@property (strong, nonatomic) UIButton *button;

@property (nonatomic) int count;

@end

@implementation callOutButtonViewController

+ (float)returnExtraPadding { return extraPadding; }

- (id)initWithType:(NSString *)type
		 withWidth:(float)width
		withHeight:(float)height
		  withIcon:(NSString *)icon
	 withIconWidth:(float)iconWidth
	withIconHeight:(float)iconHeight
setDisabledAfterPress:(BOOL)setDisabled
isSelectable:(BOOL)setSelectable {
	
	
	
	self = [super init];
	if(self) {
		self.type = type;
		self.width = width;
		self.height = height;
		self.icon = icon;
		self.iconWidth = iconWidth;
		self.iconHeight = iconHeight;
		self.widthEP = self.width + extraPadding;
		self.heightEP = self.height + extraPadding;
		self.particles = [[NSMutableArray alloc] init];
		self.setDisabled = setDisabled;
		self.setSelectable = setSelectable;
		self.animationInProgress = NO;
		self.count = 0;
		
		dispatch_once(&onceToken, ^{
			//from left to right particles in order as they appear in the effect
			particleXLocation = @[@(-5), @(20), @(35)];
			particleYLocation = @[@(25), @(0), @(0)];
			particleDelay = @[@(0.25), @(0.15), @(0.3)];
			particleXDeviate = @[@(-2.0), @(-1.0), @(1.5)];
			particleYDeviate = @[@(10.0), @(16.0), @(8.0)];
			
			iconTopGradientColor = [UIColor colorWithRed:0xff/255.0f green:0xdd/255.0f blue:0x55/255.0f alpha:1];
			iconBottomGradientColor = [UIColor colorWithRed:0xec/255.0f green:0xa0/255.0f blue:0x43/255.0f alpha:1];
			
			iconDisabledTopGradientColor = [UIColor colorWithRed:0xff/255.0f green:0xdd/255.0f blue:0x55/255.0f alpha:0.3];
			iconDisabledBottomGradientColor = [UIColor colorWithRed:0xec/255.0f green:0xa0/255.0f blue:0x43/255.0f alpha:0.3];
			
			iconSelectedTopGradientColor = [UIColor colorWithRed:0x255/255.0f green:0x255/255.0f blue:0x255/255.0f alpha:1.0];
			iconSelectedBottomGradientColor = [UIColor colorWithRed:0x255/255.0f green:0x255/255.0f blue:0x255/255.0f alpha:1.0];
			
			iconGradientColors = [NSArray arrayWithObjects:(id)iconTopGradientColor.CGColor,(id)iconBottomGradientColor.CGColor, nil];
			iconGradientColorsDisabled = [NSArray arrayWithObjects:(id)iconDisabledTopGradientColor.CGColor,(id)iconDisabledBottomGradientColor.CGColor, nil];
			iconGradientColorsSelected = [NSArray arrayWithObjects:(id)iconSelectedTopGradientColor.CGColor,(id)iconSelectedBottomGradientColor.CGColor, nil];
			
			backgroundColor = [UIColor colorWithRed:0xff/255.0f green:0xba/255.0f blue:0x00/255.0f alpha:1.0];
			
		});
		
	}
	return self;
}

-(void)loadView {
	self.view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.widthEP, self.heightEP)];
}

- (void)viewDidLoad {
    [super viewDidLoad];
	self.container = [UIButton buttonWithType:UIButtonTypeCustom];
	self.container.frame = CGRectMake(0, 0, CGRectGetWidth(self.view.bounds), CGRectGetHeight(self.view.bounds));
	[self.container addTarget:self action:@selector(buttonPressed:) forControlEvents:UIControlEventTouchUpInside];
	[self.view addSubview:self.container];
	[self setupLayers];
}

- (void)viewDidAppear:(BOOL)animated {
	[super viewDidAppear:animated];
	
}

- (void)setupLayers {
	
	self.borderForLayout = [CALayer layer];
	self.borderForLayout.frame = CGRectMake(0, 0, self.widthEP, self.heightEP);
	self.borderForLayout.borderColor = [UIColor yellowColor].CGColor;
	self.borderForLayout.borderWidth = 1.0;
	//[self.container.layer addSublayer:self.borderForLayout];
	
    
    self.scaleHolder = [CALayer layer];
    self.scaleHolder.frame = CGRectMake(0, 0, self.widthEP, self.heightEP);
    [self.container.layer addSublayer:self.scaleHolder];
    
    
    
    self.secondScale = [CALayer layer];
	self.secondScale.backgroundColor= [UIColor colorWithRed:0x255/255.0f green:0x240/255.0f blue:0x7/255.0f alpha:0.8].CGColor;
	self.secondScale.opacity = .8;
	self.secondScale.frame = CGRectMake((self.widthEP/2) - (self.width/2),
										(self.heightEP/2) - (self.height/2),
										self.width,
										self.height);
	self.secondScale.transform = CATransform3DScale(CATransform3DIdentity, scaleReducer, scaleReducer, scaleReducer);
	self.secondScale.cornerRadius = self.width/2;
	self.secondScale.shadowOffset = CGSizeMake(0, 0);
	self.secondScale.shadowRadius = 3.0;
	self.secondScale.shadowColor = [UIColor redColor].CGColor;
	self.secondScale.shadowOpacity = 0.5;
	[self.scaleHolder addSublayer:self.secondScale];
    
    
	
	self.firstScale = [CALayer layer];
	self.firstScale.backgroundColor= [UIColor whiteColor].CGColor;
	self.firstScale.opacity = .8;
	self.firstScale.frame = CGRectMake((self.widthEP/2) - (self.width/2),
									   (self.heightEP/2) - (self.height/2),
									   self.width,
									   self.height);
	self.firstScale.transform = CATransform3DScale(CATransform3DIdentity, 0.8, 0.8, 0.8);
	self.firstScale.cornerRadius = self.width/2;
	self.firstScale.shadowOffset = CGSizeMake(0, 0);
	self.firstScale.shadowRadius = 3.0;
	self.firstScale.shadowColor = [UIColor whiteColor].CGColor;
	self.firstScale.shadowOpacity = 1.0;
	[self.scaleHolder addSublayer:self.firstScale];
	
	
    self.scaleMask = [CALayer layer];
    CGRect c1 = CGRectMake(0, 0, self.widthEP, self.heightEP);
    CGRect c2 = CGRectMake((self.widthEP/2) - (self.width/2),
                           (self.heightEP/2) - (self.height/2),
						   self.width,
                           self.height);

    UIGraphicsBeginImageContextWithOptions(c1.size, NO, 0);
    CGContextRef c = UIGraphicsGetCurrentContext();
    CGContextAddEllipseInRect(c, c2);
    CGContextAddRect(c, c1);
    CGContextEOClip(c);
    CGContextSetFillColorWithColor(c, [UIColor blackColor].CGColor);
    CGContextFillRect(c, c1);
    UIImage *dynamicMask = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
	
    self.scaleMask.frame = CGRectMake(0, 0, self.widthEP, self.heightEP);
    self.scaleMask.contents = (id)dynamicMask.CGImage;
    self.scaleHolder.mask = self.scaleMask;

    

	self.background = [CALayer layer];
	self.background.backgroundColor = backgroundColor.CGColor;
	self.background.opacity = 0.3;
	self.background.frame = CGRectMake((self.widthEP/2) - (self.width/2),
									   (self.heightEP/2) - (self.height/2),
									   self.width,
									   self.height);
	self.background.cornerRadius = self.width/2;
	[self.container.layer addSublayer:self.background];
    
    self.iconBackground = [CAGradientLayer layer];
    self.iconBackground.colors = iconGradientColors;
    self.iconBackground.frame = CGRectMake((self.widthEP/2) - (self.iconWidth/2),
                                           (self.heightEP/2) - (self.iconHeight/2),
                                           self.iconWidth, self.iconHeight);
    [self.container.layer addSublayer:self.iconBackground];
    
    self.iconImage = [CALayer layer];
    self.iconImage.contents = (id)[[UIImage imageNamed:self.icon] CGImage];
    self.iconImage.frame = self.iconBackground.bounds;
    self.iconBackground.mask = self.iconImage;
    self.iconBackground.masksToBounds = YES;
    
    
    //hardcoded 52 needs to refactored.  Need to figure out with design how many tail images they want to supply
	//52x52 is the tail provided which fits with the self.width and self.height of 45x45 default.
    
    self.tail = [CALayer layer];
    self.tail.opacity = 0.0;
	self.tail.frame = CGRectMake((self.widthEP/2) - (52/2),
                                 (self.heightEP/2) - (52/2),
                                 52,
                                 52);
    self.tail.contents = (id)[[UIImage imageNamed:@"buttontail2.png"] CGImage];
	[self.container.layer addSublayer:self.tail];
    
	
	self.shineContainer = [CALayer layer];
    self.shineContainer.opacity = .5;
	//self.shineContainer.backgroundColor = [UIColor purpleColor].CGColor;
    self.shineContainer.frame = CGRectMake(0, 0, self.widthEP, self.heightEP);
    [self.container.layer addSublayer:self.shineContainer];
	
    
    self.shineMovingHolderInsideContainer = [CALayer layer];
    self.shineMovingHolderInsideContainer.opacity = .5;
//	shineMovingHolderInsideContainer = [UIColor yellowColor].CGColor;
//	shineMovingHolderInsideContainer = 1;
    self.shineMovingHolderInsideContainer.frame = CGRectMake(-self.widthEP, -self.heightEP, self.widthEP, self.heightEP);
    [self.shineContainer addSublayer:self.shineMovingHolderInsideContainer];
    
    
    
    UIColor *topGradientColor = [UIColor colorWithRed:0x255/255.0f green:0x255/255.0f blue:0x255/255.0f alpha:0.0];
    UIColor *middleGradientColor = [UIColor colorWithRed:0xff/255.0f green:0xdd/255.0f blue:0x55/255.0f alpha:0.9];
    UIColor *bottomGradientColor = [UIColor colorWithRed:0x255/255.0f green:0x255/255.0f blue:0x255/255.0f alpha:0.0];
    
    self.shine = [CAGradientLayer layer];
    self.shine.colors = [NSArray arrayWithObjects:(id)topGradientColor.CGColor,(id)middleGradientColor.CGColor,(id)bottomGradientColor.CGColor, nil];
    self.shine.anchorPoint = CGPointMake(1.0, 0.5);
    
    
	//since the shinewidth is at an angle we want its width to always be the length from one corner to the other
    float shineWidth = sqrtf( 2 * (self.widthEP * self.widthEP));
    float shineXPostion = self.widthEP - shineWidth;
	//shineheight is a hardcoded number for now, it equals 25
	float shineHeight = 25;
    
    self.shine.frame = CGRectMake(shineXPostion, -shineHeight/2, shineWidth, shineHeight);
    self.shine.transform = CATransform3DMakeRotation(-45.0/180 * M_PI, 0.0, 0.0,  1.0);
    [self.shineMovingHolderInsideContainer addSublayer:self.shine];
    
    
    self.shineMask = [CALayer layer];
	//this int takes account for adding extra room for the shine to be seen over the glow scaling border
	float shineMaskExtraSize = 8;
	self.shineMask.backgroundColor = [UIColor purpleColor].CGColor;
	self.shineMask.frame = CGRectMake(
										( (self.widthEP/2) - (self.width/2) - (shineMaskExtraSize/2)),
									    ( (self.heightEP/2) - (self.height/2) - (shineMaskExtraSize/2)),
									   self.width + shineMaskExtraSize,
									   self.height + shineMaskExtraSize);
	self.shineMask.cornerRadius = (self.width + shineMaskExtraSize)/2;
    self.shineContainer.mask = self.shineMask;
    
    
	
	
	
	self.particleHolder = [CALayer layer];
//	self.particleHolder.borderColor = [UIColor redColor].CGColor;
//	self.particleHolder.borderWidth = 1;
	self.particleHolder.frame = CGRectMake((self.widthEP/2) - (self.width/2),
									   (self.heightEP/2) - (self.height/2),
									   self.width,
									   self.height);
	[self.container.layer addSublayer:self.particleHolder];
	
	//setup all initial animations
	for(int i = 0; i < 3; i++) {
		[self makeParticleWithX:[particleXLocation[i] floatValue] withY:[particleYLocation[i] floatValue] index:i];
	}
    
    self.tailFadeIn = [CABasicAnimation animationWithKeyPath:@"opacity"];
    [self.tailFadeIn setValue:@"tailAnimateIn" forKey:@"animation1"];
    self.tailFadeIn.duration = 0.3;
    self.tailFadeIn.delegate = self;
    self.tailFadeIn.fromValue = [NSNumber numberWithFloat:0.0f];
    self.tailFadeIn.toValue = [NSNumber numberWithFloat:1.0f];
    self.tailFadeIn.removedOnCompletion = NO;
    self.tailFadeIn.fillMode = kCAFillModeForwards;
	
	self.tailAnimate = [CABasicAnimation animationWithKeyPath:@"transform.rotation"];
	[self.tailAnimate setValue:@"tailRotate" forKey:@"animation2"];
	self.tailAnimate.fromValue = [NSNumber numberWithFloat:0.0f];
	self.tailAnimate.toValue = [NSNumber numberWithFloat:((360*M_PI)/180)];
	self.tailAnimate.duration = 0.3f;
	self.tailAnimate.delegate = self;
	self.tailAnimate.removedOnCompletion = NO;
	self.tailAnimate.fillMode = kCAFillModeForwards;
	
	self.tailFadeOut = [CABasicAnimation animationWithKeyPath:@"opacity"];
    [self.tailFadeOut setValue:@"tailAnimateOut" forKey:@"animation3"];
    self.tailFadeOut.duration = 0.3;
    self.tailFadeOut.delegate = self;
    self.tailFadeOut.fromValue = [NSNumber numberWithFloat:1.0f];
    self.tailFadeOut.toValue = [NSNumber numberWithFloat:0.0f];
    self.tailFadeOut.removedOnCompletion = NO;
    self.tailFadeOut.fillMode = kCAFillModeForwards;
	
	self.shineAnimate = [CABasicAnimation animationWithKeyPath:@"transform"];
	[self.shineAnimate setValue:@"shineAnimate" forKey:@"animation6"];
	self.shineAnimate.duration = 0.8;
	self.shineAnimate.delegate = self;
	self.shineAnimate.removedOnCompletion = NO;
	[self.shineAnimate setFillMode:kCAFillModeForwards];
	CATransform3D xform = CATransform3DIdentity;
	CATransform3D xform1 = CATransform3DIdentity;
	xform1 = CATransform3DTranslate(xform, 0.0, 0.0, 0.0);
	xform = CATransform3DTranslate(xform, (self.width + extraPadding) * 2, (self.height + extraPadding) * 2, 0.0);
	self.shineAnimate.fromValue = [NSValue valueWithCATransform3D:xform1];
	self.shineAnimate.toValue = [NSValue valueWithCATransform3D:xform];
}


//ENDING DELEGATE
- (void)animationDidStop:(CAAnimation *)anim finished:(BOOL)flag{
    if([[anim valueForKey:@"animation5"] isEqual:@"yellowScaleIn"]) {
		self.whiteFadeOut = [CABasicAnimation animationWithKeyPath:@"opacity"];
		[self.whiteFadeOut setValue:@"whiteFadeOut" forKey:@"animation7"];
		self.whiteFadeOut.duration = 0.2;
		self.whiteFadeOut.delegate = self;
		self.whiteFadeOut.fromValue = [NSNumber numberWithFloat:0.8f];
		self.whiteFadeOut.toValue = [NSNumber numberWithFloat:0.0f];
		self.whiteFadeOut.removedOnCompletion = NO;
		self.whiteFadeOut.fillMode = kCAFillModeForwards;

		
		self.yellowFadeOut = [CABasicAnimation animationWithKeyPath:@"opacity"];
		[self.yellowFadeOut setValue:@"yellowFadeOut" forKey:@"animation8"];
		self.yellowFadeOut.duration = 0.2;
		self.yellowFadeOut.delegate = self;
		self.yellowFadeOut.fromValue = [NSNumber numberWithFloat:0.8f];
		self.yellowFadeOut.toValue = [NSNumber numberWithFloat:0.0f];
		self.yellowFadeOut.removedOnCompletion = NO;
		self.yellowFadeOut.fillMode = kCAFillModeForwards;
		
		
		[self.whiteFadeOut setBeginTime:CACurrentMediaTime() + 0.2];
		[self.firstScale addAnimation:self.whiteFadeOut forKey:@"animation7"];
		
		[self.yellowFadeOut setBeginTime:CACurrentMediaTime() + 0.2];
		[self.secondScale addAnimation:self.yellowFadeOut forKey:@"animation8"];
		
    }
	
	if([[anim valueForKey:@"animation8"] isEqual:@"yellowFadeOut"]) {
		[self.secondScale removeAllAnimations];
		[self.firstScale removeAllAnimations];
		self.firstScale.opacity = 0.8;
		self.secondScale.opacity = 0.8;
		self.firstScale.transform = CATransform3DScale(CATransform3DIdentity, scaleReducer, scaleReducer, scaleReducer);
		self.secondScale.transform = CATransform3DScale(CATransform3DIdentity, scaleReducer, scaleReducer, scaleReducer);
		
		//the selected/disabled animation group here
		if(self.setDisabled) {
			[self disabled:NO];
		} else if(self.setSelectable) {
			if(!self.isSelected) {
				self.selectedBackground = [CABasicAnimation animationWithKeyPath:@"opacity"];
				[self.selectedBackground setValue:@"selectedBackground" forKey:@"animation13"];
				self.selectedBackground.duration = 0.3;
				self.selectedBackground.delegate = self;
				self.selectedBackground.fromValue = [NSNumber numberWithFloat:0.3f];
				self.selectedBackground.toValue = [NSNumber numberWithFloat:1.0f];
				self.selectedBackground.removedOnCompletion = NO;
				self.selectedBackground.fillMode = kCAFillModeForwards;
				[self.background addAnimation:self.selectedBackground forKey:@"animation13"];
				
				self.selectedIcon = [CABasicAnimation animationWithKeyPath:@"colors"];
				[self.selectedIcon setValue:@"selectedIcon" forKey:@"animation15"];
				self.selectedIcon.duration = 0.3;
				self.selectedIcon.delegate = self;
				self.selectedIcon.fromValue = iconGradientColors;
				self.selectedIcon.toValue = iconGradientColorsSelected;
				self.selectedIcon.removedOnCompletion = NO;
				self.selectedIcon.fillMode = kCAFillModeForwards;
				[self.iconBackground addAnimation:self.selectedIcon forKey:@"animation15"];
				
			}
		} else {
			//fallback to here when disabled and selectable aren't set, not sure how often this will happen
			self.animationInProgress = NO;
		}
	}
	
	//deselectedBackground brought back in
	if([[anim valueForKey:@"animation13"] isEqual:@"selectedBackground"]) {
		self.isSelected = YES;
		self.animationInProgress = NO;
	}
	
	//deselected icon brought back in
	if([[anim valueForKey:@"animation14"] isEqual:@"originalBackground"]) {
		self.isSelected = NO;
		self.animationInProgress = NO;
	}
}

- (void)buttonPressed:(id)sender {
	
	if(self.animationInProgress) {
		return;
	}else {
		self.animationInProgress = YES;
	}
	
	if(self.setDisabled) {
		if(!self.isDisabled) {
			self.container.userInteractionEnabled = NO;
			self.isDisabled = YES;
		}else {
			return;
		}
	}else if(self.isSelected) {
		self.selectedBackground = [CABasicAnimation animationWithKeyPath:@"opacity"];
		[self.selectedBackground setValue:@"originalBackground" forKey:@"animation14"];
		self.selectedBackground.duration = 0.3;
		self.selectedBackground.delegate = self;
		self.selectedBackground.fromValue = [NSNumber numberWithFloat:1.0f];
		self.selectedBackground.toValue = [NSNumber numberWithFloat:0.3f];
		self.selectedBackground.removedOnCompletion = NO;
		self.selectedBackground.fillMode = kCAFillModeForwards;
		[self.background addAnimation:self.selectedBackground forKey:@"animation14"];
		
		self.deselectedIcon = [CABasicAnimation animationWithKeyPath:@"colors"];
		[self.deselectedIcon setValue:@"deselectedIcon" forKey:@"animation16"];
		self.deselectedIcon.duration = 0.3;
		self.deselectedIcon.delegate = self;
		self.deselectedIcon.fromValue = iconGradientColorsSelected;
		self.deselectedIcon.toValue = iconGradientColors;
		self.deselectedIcon.removedOnCompletion = NO;
		self.deselectedIcon.fillMode = kCAFillModeForwards;
		[self.iconBackground addAnimation:self.deselectedIcon forKey:@"animation16"];
		
		return;
	}
	
	[self primaryAnimations];
}

- (void)makeParticleWithX:(float)xPos withY:(float)yPos index:(int)index {
	//CALayer *n = (CALayer *)[self.particles objectAtIndex:index];
	//CALayer *n = [self.particles objectAtIndex:index];
	spark *n = [spark layer];
	n.opacity = 0.0;
	//n.backgroundColor = [UIColor whiteColor].CGColor;
	//n.cornerRadius = 4/2;
	n.shadowOffset = CGSizeMake(0, 0);
	n.shadowRadius = 3.0;
	n.shadowColor = [UIColor whiteColor].CGColor;
	n.shadowOpacity = 3.0;
	n.frame = CGRectMake(xPos, yPos, 10, 10);
	[n drawSpark:10];
	//NSLog(@"%f",xPos);
	[self.particles addObject:n];
	[self.particleHolder addSublayer:n];
}

- (void)disabled:(BOOL)externalCall {
	if(externalCall) {
		self.storedDisabled = self.setDisabled;
		self.storedEnabled = self.isDisabled;
		self.setDisabled = YES;
		self.isDisabled = YES;
	}
	self.disabledBackground = [CABasicAnimation animationWithKeyPath:@"opacity"];
	[self.disabledBackground setValue:@"disabledSelectedBackground" forKey:@"animation11"];
	self.disabledBackground.duration = 0.3;
	self.disabledBackground.delegate = self;
	self.disabledBackground.fromValue = [NSNumber numberWithFloat:0.3f];
	self.disabledBackground.toValue = [NSNumber numberWithFloat:0.1f];
	self.disabledBackground.removedOnCompletion = NO;
	self.disabledBackground.fillMode = kCAFillModeForwards;
	[self.background addAnimation:self.disabledBackground forKey:@"animation11"];
	
	self.disabledIcon = [CABasicAnimation animationWithKeyPath:@"colors"];
	[self.disabledIcon setValue:@"disabledSelectedIcon" forKey:@"animation12"];
	self.disabledIcon.duration = 0.3;
	self.disabledIcon.delegate = self;
	self.disabledIcon.fromValue = iconGradientColors;
	self.disabledIcon.toValue = iconGradientColorsDisabled;
	self.disabledIcon.removedOnCompletion = NO;
	self.disabledIcon.fillMode = kCAFillModeForwards;
	[self.iconBackground addAnimation:self.disabledIcon forKey:@"animation12"];
}


- (void)enabled {
	self.setDisabled = self.storedDisabled;
	self.isDisabled = self.storedEnabled;
	[self.background removeAnimationForKey:@"animation11"];
	[self.iconBackground removeAnimationForKey:@"animation12"];
	self.container.userInteractionEnabled = YES;
	self.animationInProgress = NO;
}


- (void)primaryAnimations {	
	self.count = 0;
	
	[self.tail addAnimation:self.tailFadeIn forKey:@"animation1"];
	[self.tail addAnimation:self.tailAnimate forKey:@"animation2"];
	[self.tail addAnimation:self.tailFadeOut forKey:@"animation3"];
	
	CAMediaTimingFunction *tf = [CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseIn];
	self.whiteScaleIn = [CABasicAnimation animationWithKeyPath:@"transform.scale"];
	[self.whiteScaleIn setValue:@"whiteScaleIn" forKey:@"animation4"];
	self.whiteScaleIn.fromValue = [NSNumber numberWithFloat:0.8];
	self.whiteScaleIn.toValue = [NSNumber numberWithFloat:1.05];
	self.whiteScaleIn.duration = 0.3;
	self.whiteScaleIn.delegate = self;
	self.whiteScaleIn.removedOnCompletion = NO;
	self.whiteScaleIn.fillMode = kCAFillModeForwards;
	self.whiteScaleIn.timingFunction = tf;
	[self.firstScale addAnimation:self.whiteScaleIn forKey:@"animation4"];
    
	self.yellowScaleIn = [CABasicAnimation animationWithKeyPath:@"transform.scale"];
	[self.yellowScaleIn setValue:@"yellowScaleIn" forKey:@"animation5"];
	self.yellowScaleIn.fromValue = [NSNumber numberWithFloat:0.8];
	self.yellowScaleIn.toValue = [NSNumber numberWithFloat:1.1];
	self.yellowScaleIn.duration = 0.35;
	self.yellowScaleIn.delegate = self;
	self.yellowScaleIn.removedOnCompletion = NO;
	self.yellowScaleIn.fillMode = kCAFillModeForwards;
	self.yellowScaleIn.timingFunction = tf;
	[self.secondScale addAnimation:self.yellowScaleIn forKey:@"animation5"];
	
	[self.shineMovingHolderInsideContainer addAnimation:self.shineAnimate forKey:@"animation6"];
	
	//set up particles and add group anaimtion to them
	for(int i = 0; i < 3; i++) {
		CALayer *n = (CALayer *)[self.particles objectAtIndex:i];
		self.particleFadeIn = [CABasicAnimation animationWithKeyPath:@"opacity"];
		self.particleFadeIn.duration = 0.15;
		self.particleFadeIn.delegate = self;
		[self.particleFadeIn setFromValue:[NSNumber numberWithFloat:0.0]];
		[self.particleFadeIn setToValue:[NSNumber numberWithFloat:0.7]];
		self.particleFadeIn.fillMode = kCAFillModeForwards;
		self.particleFadeIn.removedOnCompletion = NO;
		
		
		self.particleMove = [CABasicAnimation animationWithKeyPath:@"position"];
		//do all this weirdness because layers anchor point is in the middle, you can move it to top left,
		//but then scaling or rotation will not happen in the middle
		float xPos = [particleXLocation[self.count] floatValue] + (n.frame.size.width/2);
		float yPos = [particleYLocation[self.count] floatValue] + (n.frame.size.height/2);
		float xDeviate = [particleXDeviate[self.count] floatValue];
		float yDeviate = [particleYDeviate[self.count] floatValue];
		[self.particleMove setFromValue:[NSValue valueWithCGPoint:CGPointMake(xPos+=xDeviate, yPos)]];
		[self.particleMove setToValue:[NSValue valueWithCGPoint:CGPointMake(xPos+=xDeviate, yPos-=yDeviate)]];
		self.particleMove.fillMode = kCAFillModeForwards;
		self.particleMove.removedOnCompletion = NO;
		
		self.particleRotate = [CABasicAnimation animationWithKeyPath:@"transform.rotation"];
		self.particleRotate.autoreverses = NO;
		self.particleRotate.fromValue = [NSNumber numberWithFloat: 0];
		self.particleRotate.toValue = [NSNumber numberWithFloat:((180*M_PI)/ -180)];
		self.particleRotate.fillMode = kCAFillModeForwards;
		self.particleRotate.removedOnCompletion = NO;
		
		self.particleFadeOut = [CABasicAnimation animationWithKeyPath:@"opacity"];
		self.particleFadeOut.beginTime = 0.15;
		self.particleFadeOut.duration = 0.15;
		[self.particleFadeOut setFromValue:[NSNumber numberWithFloat:0.7]];
		[self.particleFadeOut setToValue:[NSNumber numberWithFloat:0.0]];
		self.particleFadeOut.fillMode = kCAFillModeForwards;
		self.particleFadeOut.removedOnCompletion = NO;
		
		self.particleGroup = [CAAnimationGroup animation];
		[self.particleGroup setValue:@"particleGroupMoveAndFade" forKey:@"animation10"];
		self.particleGroup.fillMode = kCAFillModeForwards;
		self.particleGroup.removedOnCompletion = NO;
		self.particleGroup.delegate = self;
		self.particleGroup.animations = @[self.particleFadeIn, self.particleMove, self.particleRotate, self.particleFadeOut];
		[self.particleGroup setBeginTime:CACurrentMediaTime() + [particleDelay[self.count] floatValue]];
		self.particleGroup.duration = 0.3;
		[n addAnimation:self.particleGroup forKey:@"animation10"];
		
		self.count++;
	}
}



@end
