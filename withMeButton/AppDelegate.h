//
//  AppDelegate.h
//  withMeButton
//
//  Created by Justin Kempton on 7/22/13.
//  Copyright (c) 2013 Justin Kempton. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ViewController;

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) ViewController *viewController;

@end
