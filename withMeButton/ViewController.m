#import "ViewController.h"

@interface ViewController ()

@property (strong, nonatomic) callOutButtonViewController *callOutButtonChat;
@property (strong, nonatomic) callOutButtonViewController *callOutButtonFriend;
@property (strong, nonatomic) callOutButtonViewController *callOutButtonFriend1;

@end

@implementation ViewController

- (void)viewDidLoad
{
	
	//just making temp vars so i can place in the center of the screen
	float tempWidthVar = 45;
	float tempHeightVar = 45;
	
    [super viewDidLoad];
	
	//Passing setDisabledAfterPress to yes always trumps isSelectable no matter what
	
	self.callOutButtonFriend = [[callOutButtonViewController alloc] initWithType:@"primary"
																	   withWidth:tempWidthVar
																	  withHeight:tempHeightVar
																		withIcon:@"iconAddFriend.png"
																   withIconWidth:25
																  withIconHeight:17
														   setDisabledAfterPress:NO
																	isSelectable:YES];
	
	
	self.callOutButtonChat = [[callOutButtonViewController alloc] initWithType:@"primary"
																 withWidth:tempWidthVar
																withHeight:tempHeightVar
																  withIcon:@"iconChat.png"
															 withIconWidth:25
															withIconHeight:17
														 setDisabledAfterPress:YES
																  isSelectable:NO];
	
	self.callOutButtonFriend1 = [[callOutButtonViewController alloc] initWithType:@"primary"
																	   withWidth:65
																	  withHeight:65
																		withIcon:@"iconAddFriend.png"
																   withIconWidth:25
																  withIconHeight:17
														   setDisabledAfterPress:NO
																	isSelectable:YES];
	

	CGRect callOutButtonFrame = self.callOutButtonFriend.view.frame;
	callOutButtonFrame.origin.x = self.view.frame.size.width/2 - (tempWidthVar + [callOutButtonViewController returnExtraPadding])/2;
	callOutButtonFrame.origin.y = self.view.frame.size.height/2 - (tempWidthVar + [callOutButtonViewController returnExtraPadding])/2;
	self.callOutButtonFriend.view.frame = callOutButtonFrame;
	[self.view addSubview:self.callOutButtonFriend.view];
	
	
	callOutButtonFrame.origin.x = self.view.frame.size.width/2 - (tempWidthVar + [callOutButtonViewController returnExtraPadding])/2;
	callOutButtonFrame.origin.y = self.view.frame.size.height/2 - (tempWidthVar + [callOutButtonViewController returnExtraPadding])/2 + 65;
	self.callOutButtonChat.view.frame = callOutButtonFrame;
	//can call disabled at anytime to disable the button yourself, however if the param setDisabledAfterPress is YES it will disabled after the press
	//if you call this manually here you will need to call enabled manually like below
	[self.callOutButtonChat disabled:YES];
	[self.view addSubview:self.callOutButtonChat.view];

	callOutButtonFrame.origin.x = self.view.frame.size.width/2 - (tempWidthVar + [callOutButtonViewController returnExtraPadding])/2;
	callOutButtonFrame.origin.y = self.view.frame.size.height/2 - (tempWidthVar + [callOutButtonViewController returnExtraPadding])/2 + 130;
	self.callOutButtonFriend1.view.frame = callOutButtonFrame;
//	[self.view addSubview:self.callOutButtonFriend1.view];
	
}




- (IBAction)reset:(id)sender {
	[self.callOutButtonChat enabled];
}
@end
