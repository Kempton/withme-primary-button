#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>

@interface callOutButtonViewController : UIViewController

- (id)initWithType:(NSString *)type
		withWidth:(float)width
		withHeight:(float)height
		  withIcon:(NSString *)icon
	 withIconWidth:(float)iconWidth
	withIconHeight:(float)iconHeight
setDisabledAfterPress:(BOOL)setDisabled
isSelectable:(BOOL)setSelectable;

+ (float)returnExtraPadding;
- (void)disabled:(BOOL)externalCall;
- (void)enabled;

@end
